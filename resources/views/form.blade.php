
<html>
    <body>
        <form name="redirectForm" action="{{$url}}" method="POST">
            <input type="hidden" name="Data" value="{{$parameters}}">
            <input type="hidden" name="InterfaceVersion" value="HP_2.20">
            <input type="hidden" name="Seal" value="{{$sha}}">
            <noscript><input type="submit" name="Go" value="Click to continue"/></noscript>
            <script type="text/javascript"> document.redirectForm.submit(); </script>
        </form>
    </body>
</html>
