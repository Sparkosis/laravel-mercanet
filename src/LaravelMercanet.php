<?php

namespace Sparkosis\LaravelMercanet;

use Sparkosis\LaravelMercanet\Dto\BillingAddressDto;
use Sparkosis\LaravelMercanet\Dto\CustomerDto;
use Sparkosis\LaravelMercanet\Dto\DtoMapper;
use Sparkosis\LaravelMercanet\Service\Mercanet;

class LaravelMercanet
{
    /**
     * @var Mercanet
     */
    private $paymentRequest;
    /**
     * @var CustomerDto
     */
    private $customer;
    /**
     * @var BillingAddressDto
     */
    private $billing;

    public function __construct()
    {
        $paymentRequest = new Mercanet(config("laravelMercanet.secretKey"));
        $paymentRequest->setMerchantId(config("laravelMercanet.merchantId"));
        if (config("laravelMercanet.isProd")) {
            $paymentRequest->setUrl(Mercanet::PRODUCTION);
        } else {
            $paymentRequest->setUrl(Mercanet::TEST);
        }
        $paymentRequest->setKeyVersion(config("laravelMercanet.keyVersion"));
        $paymentRequest->setCurrency(config("laravelMercanet.currency"));
        $this->paymentRequest = $paymentRequest;
    }

    public function process($transactionReference, $amount)
    {
        $this->paymentRequest->setTransactionReference($transactionReference);
        $this->paymentRequest->setAmount($amount);
        $this->paymentRequest->setLanguage(app()->getLocale());
        $this->paymentRequest->setAutomaticResponseUrl(route(config('laravelMercanet.returnRoute')));
        $this->paymentRequest->setNormalReturnUrl(route(config('laravelMercanet.returnRoute')));
        $this->paymentRequest->setFraudDataBypass3DS(Mercanet::BYPASS3DS_ALL);

        if (! is_null($this->billing)) {
            DtoMapper::mapBilling($this->paymentRequest, $this->billing);
        }

        if (! is_null($this->customer)) {
            DtoMapper::mapCustomer($this->paymentRequest, $this->customer);
        }

        $this->paymentRequest->validate();
    }


    public function render()
    {
        return view("laravelMercanet::form", ['url' => $this->paymentRequest->getUrl(), "parameters" => $this->paymentRequest->toParameterString(), "sha" => $this->paymentRequest->getShaSign() ]);
    }

    public static function checkSha($sha, $params)
    {
        $parameters = [];
        $dataParams = explode('|', $params);
        foreach ($dataParams as $dataParamString) {
            $dataKeyValue = explode('=', $dataParamString, 2);
            $parameters[$dataKeyValue[0]] = $dataKeyValue[1];
        }

        $shaString = '';
        foreach ($parameters as $key => $value) {
            $shaString .= $key . '=' . $value;
            $shaString .= (array_search($key, array_keys($parameters)) != (count($parameters) - 1)) ? '|' : config("laravelMercanet.secretKey");
        }

        return hash('sha256', $shaString) == $sha && in_array($parameters['RESPONSECODE'], ["00", "60"]);
    }

    public function customer(CustomerDto $customerDto): LaravelMercanet
    {
        $this->customer = $customerDto;

        return $this;
    }

    public function billing(BillingAddressDto $billingAddressDto): LaravelMercanet
    {
        $this->billing = $billingAddressDto;

        return $this;
    }
}
