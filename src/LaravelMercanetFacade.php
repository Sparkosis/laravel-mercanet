<?php

namespace Sparkosis\LaravelMercanet;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Sparkosis\LaravelMercanet\LaravelMercanet
 */
class LaravelMercanetFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'laravelMercanet';
    }
}
