<?php


namespace Sparkosis\LaravelMercanet\Dto;

use Sparkosis\LaravelMercanet\Service\Mercanet;

class DtoMapper
{
    public static function mapBilling(Mercanet $paymentRequest, BillingAddressDto $billingAddressDto)
    {
        $paymentRequest->setBillingAddressCity($billingAddressDto->getBillingAddressCity());
        $paymentRequest->setBillingAddressStreet($billingAddressDto->getBillingAddressStreet());
        $paymentRequest->setBillingAddressStreetNumber($billingAddressDto->getBillingAddressStreetNumber());
        $paymentRequest->setBillingAddressZipCode($billingAddressDto->getBillingAddressZipCode());
        $paymentRequest->setBillingContactEmail($billingAddressDto->getBillingContactEmail());
        $paymentRequest->setBillingContactFirstname($billingAddressDto->getBillingContactFirstname());
        $paymentRequest->setBillingContactLastname($billingAddressDto->getBillingContactLastName());
        $paymentRequest->setBillingContactPhone($billingAddressDto->getBillingContactPhone());
    }

    public static function mapCustomer(Mercanet $paymentRequest, CustomerDto $customer)
    {
        $paymentRequest->setCustomerAddressCity($customer->getCustomerAddressCity());
        $paymentRequest->setCustomerAddressStreet($customer->getCustomerAddressStreet());
        $paymentRequest->setCustomerAddressStreetNumber($customer->getCustomerAddressStreetNumber());
        $paymentRequest->setCustomerAddressZipCode($customer->getCustomerAddressZipCode());
        $paymentRequest->setCustomerAddressState($customer->getCustomerAddressState());
        $paymentRequest->setCustomerAddressCompany($customer->getCustomerAddressCompany());
        $paymentRequest->setCustomerAddressCountry($customer->getCustomerAddressCountry());
        $paymentRequest->setCustomerAddressPostBox($customer->getCustomerAddressPostBox());
        $paymentRequest->setCustomerContactEmail($customer->getCustomerContactEmail());
        $paymentRequest->setCustomerContactFirstName($customer->getCustomerContactFirstname());
        $paymentRequest->setCustomerContactGender($customer->getCustomerContactGender());
        $paymentRequest->setCustomerContactLastName($customer->getCustomerContactLastname());
        $paymentRequest->setCustomerContactEmail($customer->getCustomerContactEmail());
        $paymentRequest->setCustomerContactMobile($customer->getCustomerContactMobile());
        $paymentRequest->setCustomerContactPhone($customer->getCustomerContactPhone());
        $paymentRequest->setCustomerContactTitle($customer->getCustomerContactTitle());
    }
}
