<?php


namespace Sparkosis\LaravelMercanet\Dto;

class CustomerDto
{
    private $customerAddressCity;
    private $customerAddressCompany;
    private $customerAddressCountry;
    private $customerAddressPostBox;
    private $customerAddressState;
    private $customerAddressStreet;
    private $customerAddressStreetNumber;
    private $customerAddressZipCode;
    private $customerContactEmail;
    private $customerContactGender;
    private $customerContactLastname;
    private $customerContactFirstname;
    private $customerContactMobile;
    private $customerContactPhone;
    private $customerContactTitle;

    /**
     * @return mixed
     */
    public function getCustomerContactFirstname()
    {
        return $this->customerContactFirstname;
    }

    /**
     * @param mixed $customerContactFirstname
     */
    public function setCustomerContactFirstname($customerContactFirstname): void
    {
        $this->customerContactFirstname = $customerContactFirstname;
    }

    /**
     * @return mixed
     */

    public function getCustomerAddressCity()
    {
        return $this->customerAddressCity;
    }

    /**
     * @param mixed $customerAddressCity
     */
    public function setCustomerAddressCity($customerAddressCity): void
    {
        $this->customerAddressCity = $customerAddressCity;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddressCompany()
    {
        return $this->customerAddressCompany;
    }

    /**
     * @param mixed $customerAddressCompany
     */
    public function setCustomerAddressCompany($customerAddressCompany): void
    {
        $this->customerAddressCompany = $customerAddressCompany;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddressCountry()
    {
        return $this->customerAddressCountry;
    }

    /**
     * @param mixed $customerAddressCountry
     */
    public function setCustomerAddressCountry($customerAddressCountry): void
    {
        $this->customerAddressCountry = $customerAddressCountry;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddressPostBox()
    {
        return $this->customerAddressPostBox;
    }

    /**
     * @param mixed $customerAddressPostBox
     */
    public function setCustomerAddressPostBox($customerAddressPostBox): void
    {
        $this->customerAddressPostBox = $customerAddressPostBox;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddressState()
    {
        return $this->customerAddressState;
    }

    /**
     * @param mixed $customerAddressState
     */
    public function setCustomerAddressState($customerAddressState): void
    {
        $this->customerAddressState = $customerAddressState;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddressStreet()
    {
        return $this->customerAddressStreet;
    }

    /**
     * @param mixed $customerAddressStreet
     */
    public function setCustomerAddressStreet($customerAddressStreet): void
    {
        $this->customerAddressStreet = $customerAddressStreet;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddressStreetNumber()
    {
        return $this->customerAddressStreetNumber;
    }

    /**
     * @param mixed $customerAddressStreetNumber
     */
    public function setCustomerAddressStreetNumber($customerAddressStreetNumber): void
    {
        $this->customerAddressStreetNumber = $customerAddressStreetNumber;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddressZipCode()
    {
        return $this->customerAddressZipCode;
    }

    /**
     * @param mixed $customerAddressZipCode
     */
    public function setCustomerAddressZipCode($customerAddressZipCode): void
    {
        $this->customerAddressZipCode = $customerAddressZipCode;
    }

    /**
     * @return mixed
     */
    public function getCustomerContactEmail()
    {
        return $this->customerContactEmail;
    }

    /**
     * @param mixed $customerContactEmail
     */
    public function setCustomerContactEmail($customerContactEmail): void
    {
        $this->customerContactEmail = $customerContactEmail;
    }

    /**
     * @return mixed
     */
    public function getCustomerContactGender()
    {
        return $this->customerContactGender;
    }

    /**
     * @param mixed $customerContactGender
     */
    public function setCustomerContactGender($customerContactGender): void
    {
        $this->customerContactGender = $customerContactGender;
    }

    /**
     * @return mixed
     */
    public function getCustomerContactLastname()
    {
        return $this->customerContactLastname;
    }

    /**
     * @param mixed $customerContactLastname
     */
    public function setCustomerContactLastname($customerContactLastname): void
    {
        $this->customerContactLastname = $customerContactLastname;
    }

    /**
     * @return mixed
     */
    public function getCustomerContactMobile()
    {
        return $this->customerContactMobile;
    }

    /**
     * @param mixed $customerContactMobile
     */
    public function setCustomerContactMobile($customerContactMobile): void
    {
        $this->customerContactMobile = $customerContactMobile;
    }

    /**
     * @return mixed
     */
    public function getCustomerContactPhone()
    {
        return $this->customerContactPhone;
    }

    /**
     * @param mixed $customerContactPhone
     */
    public function setCustomerContactPhone($customerContactPhone): void
    {
        $this->customerContactPhone = $customerContactPhone;
    }

    /**
     * @return mixed
     */
    public function getCustomerContactTitle()
    {
        return $this->customerContactTitle;
    }

    /**
     * @param mixed $customerContactTitle
     */
    public function setCustomerContactTitle($customerContactTitle): void
    {
        $this->customerContactTitle = $customerContactTitle;
    }
}
