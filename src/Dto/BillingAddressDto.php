<?php


namespace Sparkosis\LaravelMercanet\Dto;

class BillingAddressDto
{
    private $billingAddressCity;
    private $billingAddressCompany;
    private $billingAddressCountry;
    private $billingAddressPostBox;
    private $billingAddressState;
    private $billingAddressStreet;
    private $billingAddressStreetNumber;
    private $billingAddressZipCode;
    private $billingContactEmail;
    private $billingContactFirstname;
    private $billingContactGender;
    private $billingContactLastName;
    private $billingContactMobile;
    private $billingContactPhone;

    /**
     * @return mixed
     */
    public function getBillingAddressCity()
    {
        return $this->billingAddressCity;
    }

    /**
     * @param mixed $billingAddressCity
     */
    public function setBillingAddressCity($billingAddressCity): void
    {
        $this->billingAddressCity = $billingAddressCity;
    }

    /**
     * @return mixed
     */
    public function getBillingAddressCompany()
    {
        return $this->billingAddressCompany;
    }

    /**
     * @param mixed $billingAddressCompany
     */
    public function setBillingAddressCompany($billingAddressCompany): void
    {
        $this->billingAddressCompany = $billingAddressCompany;
    }

    /**
     * @return mixed
     */
    public function getBillingAddressCountry()
    {
        return $this->billingAddressCountry;
    }

    /**
     * @param mixed $billingAddressCountry
     */
    public function setBillingAddressCountry($billingAddressCountry): void
    {
        $this->billingAddressCountry = $billingAddressCountry;
    }

    /**
     * @return mixed
     */
    public function getBillingAddressPostBox()
    {
        return $this->billingAddressPostBox;
    }

    /**
     * @param mixed $billingAddressPostBox
     */
    public function setBillingAddressPostBox($billingAddressPostBox): void
    {
        $this->billingAddressPostBox = $billingAddressPostBox;
    }

    /**
     * @return mixed
     */
    public function getBillingAddressState()
    {
        return $this->billingAddressState;
    }

    /**
     * @param mixed $billingAddressState
     */
    public function setBillingAddressState($billingAddressState): void
    {
        $this->billingAddressState = $billingAddressState;
    }

    /**
     * @return mixed
     */
    public function getBillingAddressStreet()
    {
        return $this->billingAddressStreet;
    }

    /**
     * @param mixed $billingAddressStreet
     */
    public function setBillingAddressStreet($billingAddressStreet): void
    {
        $this->billingAddressStreet = $billingAddressStreet;
    }

    /**
     * @return mixed
     */
    public function getBillingAddressStreetNumber()
    {
        return $this->billingAddressStreetNumber;
    }

    /**
     * @param mixed $billingAddressStreetNumber
     */
    public function setBillingAddressStreetNumber($billingAddressStreetNumber): void
    {
        $this->billingAddressStreetNumber = $billingAddressStreetNumber;
    }

    /**
     * @return mixed
     */
    public function getBillingAddressZipCode()
    {
        return $this->billingAddressZipCode;
    }

    /**
     * @param mixed $billingAddressZipCode
     */
    public function setBillingAddressZipCode($billingAddressZipCode): void
    {
        $this->billingAddressZipCode = $billingAddressZipCode;
    }

    /**
     * @return mixed
     */
    public function getBillingContactEmail()
    {
        return $this->billingContactEmail;
    }

    /**
     * @param mixed $billingContactEmail
     */
    public function setBillingContactEmail($billingContactEmail): void
    {
        $this->billingContactEmail = $billingContactEmail;
    }

    /**
     * @return mixed
     */
    public function getBillingContactFirstname()
    {
        return $this->billingContactFirstname;
    }

    /**
     * @param mixed $billingContactFirstname
     */
    public function setBillingContactFirstname($billingContactFirstname): void
    {
        $this->billingContactFirstname = $billingContactFirstname;
    }

    /**
     * @return mixed
     */
    public function getBillingContactGender()
    {
        return $this->billingContactGender;
    }

    /**
     * @param mixed $billingContactGender
     */
    public function setBillingContactGender($billingContactGender): void
    {
        $this->billingContactGender = $billingContactGender;
    }

    /**
     * @return mixed
     */
    public function getBillingContactLastName()
    {
        return $this->billingContactLastName;
    }

    /**
     * @param mixed $billingContactLastName
     */
    public function setBillingContactLastName($billingContactLastName): void
    {
        $this->billingContactLastName = $billingContactLastName;
    }

    /**
     * @return mixed
     */
    public function getBillingContactMobile()
    {
        return $this->billingContactMobile;
    }

    /**
     * @param mixed $billingContactMobile
     */
    public function setBillingContactMobile($billingContactMobile): void
    {
        $this->billingContactMobile = $billingContactMobile;
    }

    /**
     * @return mixed
     */
    public function getBillingContactPhone()
    {
        return $this->billingContactPhone;
    }

    /**
     * @param mixed $billingContactPhone
     */
    public function setBillingContactPhone($billingContactPhone): void
    {
        $this->billingContactPhone = $billingContactPhone;
    }
}
