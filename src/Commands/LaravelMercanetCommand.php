<?php

namespace Sparkosis\LaravelMercanet\Commands;

use Illuminate\Console\Command;

class LaravelMercanetCommand extends Command
{
    public $signature = 'laravelMercanet';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
