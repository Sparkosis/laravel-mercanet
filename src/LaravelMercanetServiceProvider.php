<?php

namespace Sparkosis\LaravelMercanet;

use Illuminate\Support\ServiceProvider;
use Sparkosis\LaravelMercanet\Commands\LaravelMercanetCommand;

class LaravelMercanetServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/laravelMercanet.php' => config_path('laravelMercanet.php'),
            ], 'config');

            $this->publishes([
                __DIR__.'/../resources/views' => base_path('resources/views/vendor/laravelMercanet'),
            ], 'views');


            $this->commands([
                LaravelMercanetCommand::class,
            ]);
        }

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'laravelMercanet');
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/laravelMercanet.php', 'laravelMercanet');
    }
}
