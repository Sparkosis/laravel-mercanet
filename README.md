# 

[![Latest Version on Packagist](https://img.shields.io/packagist/v/sparkosis/laravelmercanet.svg?style=flat-square)](https://packagist.org/packages/sparkosis/laravelmercanet)
[![GitHub Tests Action Status](https://img.shields.io/github/workflow/status/spatie/laravelMercanet/run-tests?label=tests)](https://github.com/spatie/laravelMercanet/actions?query=workflow%3Arun-tests+branch%3Amaster)
[![Total Downloads](https://img.shields.io/packagist/dt/spatie/laravelMercanet.svg?style=flat-square)](https://packagist.org/packages/sparkosis/laravelmercanet)


This is where your description should go. Limit it to a paragraph or two. Consider adding a small example.


## Installation

You can install the package via composer:

```bash
composer require sparkosis/laravel-mercanet
```


You can publish the config file with:
```bash
php artisan vendor:publish --provider="Sparkosis\LaravelMercanet\LaravelMercanetServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
    "merchantId" => "211000021310001",
    "isProd" => false,
    "keyVersion" => 1,
    "secretKey" => 'S9i8qClCnb2CZU3y3Vn0toIOgz3z_aBi79akR30vM9o',
    "returnRoute" => 'mercanet.normalReturn',
];
```

## Usage
###Send a payment
``` php
  $transaction = new Sparkosis\LaravelMercanet\LaravelMercanet();
  $transaction->process("trans-001", 5000); //TransactionReference and amount in cents
```
###Check payment
``` php
 $checkShaIsOk = \Sparkosis\LaravelMercanet\LaravelMercanet::checkSha($request->input("Seal"), $request->input("Data"));
```
## Testing

``` bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email freek@spatie.be instead of using the issue tracker.

## Credits

- [Nicolas Ramos](https://github.com/NicolasRamos)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
